package util

import (
	"context"
	"net"

	"github.com/digitalocean/godo"
	"golang.org/x/oauth2"

	log "github.com/sirupsen/logrus"
)

type tokenSource struct {
	AccessToken string
}

func (t *tokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}

	return token, nil
}

// DoClient returns a digitalocean client, starting from a token.
func DoClient(token string) *godo.Client {
	tokenSource := &tokenSource{
		AccessToken: token,
	}

	oauthClient := oauth2.NewClient(context.Background(), tokenSource)

	return godo.NewClient(oauthClient)
}

// DomainRecords requests the current dns records in digitalocean for a given domain.
func DomainRecords(doctx context.Context, client *godo.Client, domain string) ([]godo.DomainRecord, error) {
	// create a list to hold our droplets
	list := []godo.DomainRecord{}

	// create options. initially, these will be blank
	opt := &godo.ListOptions{}

	for {
		records, resp, err := client.Domains.Records(doctx, domain, opt)
		if err != nil {
			return nil, err
		}

		// append the current page's records to our list
		list = append(list, records...)

		// if we are at the last page, break out the for loop
		if resp.Links == nil || resp.Links.IsLastPage() {
			break
		}

		page, err := resp.Links.CurrentPage()
		if err != nil {
			return nil, err
		}

		// set the page we want for the next request
		opt.Page = page + 1
	}

	return list, nil
}

// UpdateDigitalOcean updates the digitalocean dns records.
func UpdateDigitalOcean(token, domain, hostname string, values map[string][]net.IP) error { //nolint:funlen
	client := DoClient(token)
	doctx := context.TODO()

	records, err := DomainRecords(doctx, client, domain)
	if err != nil {
		return err
	}

	for _, record := range records {
		if record.Name != hostname {
			continue
		}

		if _, ok := values[record.Type]; !ok {
			continue
		}

		if len(values[record.Type]) > 0 {
			var value net.IP
			value, values[record.Type] = values[record.Type][0], values[record.Type][1:]

			request := &godo.DomainRecordEditRequest{
				Data: value.String(),
				TTL:  60,
			}

			log.Printf("Digitalocean: set record %d %s %s", record.ID, record.Type, value)

			if _, _, err := client.Domains.EditRecord(doctx, domain, record.ID, request); err != nil {
				return err
			}
		} else {
			log.Printf("Digitalocean: del record %d %s %s", record.ID, record.Type, record.Data)

			if _, err := client.Domains.DeleteRecord(doctx, domain, record.ID); err != nil {
				return err
			}
		}
	}

	for typ, records := range values {
		for len(records) > 0 {
			var value net.IP
			value, records = records[0], records[1:]

			request := &godo.DomainRecordEditRequest{
				Type: typ,
				Name: hostname,
				Data: value.String(),
				TTL:  60,
			}

			log.Printf("Digitalocean: add record %s %s %s", hostname, typ, value)

			if _, _, err := client.Domains.CreateRecord(doctx, domain, request); err != nil {
				return err
			}
		}
	}

	return nil
}
