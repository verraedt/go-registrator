package registrator

import (
	"net"
	"time"

	"gitlab.com/verraedt/go-registrator/util"

	log "github.com/sirupsen/logrus"
)

func (reg *Registrator) RegisterDNS(ips []net.IP) error {
	if reg.Config.DoToken == "" || reg.Config.DoDomain == "" {
		return nil
	}

	log.Infof("Setting public addresses: %v", ips)

	values, err := util.GetAddressRecords(ips)
	if err != nil {
		return err
	}

	count := 0

	for {
		err = util.UpdateDigitalOcean(reg.Config.DoToken, reg.Config.DoDomain, reg.Config.Hostname, values)
		if err == nil {
			return nil
		}

		count++

		if count > 3 {
			return err
		}

		time.Sleep(time.Second)
	}
}
