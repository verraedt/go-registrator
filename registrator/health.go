package registrator

import (
	"fmt"
	"net/http"
	"strings"

	dockerapi "github.com/fsouza/go-dockerclient"
)

// ServeHealth spins up a HTTP server that can be used to check the health of docker containers.
func (reg *Registrator) ServeHealth() error {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		path = strings.TrimPrefix(path, "/")

		container, err := reg.docker.InspectContainerWithOptions(dockerapi.InspectContainerOptions{
			ID:      path,
			Context: reg.Deadline(),
		})
		switch {
		case err != nil:
			http.Error(w, "container not found", http.StatusNotFound)
		case !container.State.Running:
			http.Error(w, "container is stopped", http.StatusServiceUnavailable)
		case container.State.Health.Status == "":
			http.Error(w, "container has no healthcheck", http.StatusNotFound)
		case container.State.Health.Status != "healthy":
			http.Error(w, fmt.Sprintf("container is %s", container.State.Health.Status), http.StatusServiceUnavailable)
		default:
			http.Error(w, fmt.Sprintf("container is %s", container.State.Health.Status), http.StatusNoContent)
		}
	})

	return http.ListenAndServe(":8505", nil)
}
