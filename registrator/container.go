package registrator

import (
	"fmt"
	"net"
	"regexp"
	"strconv"
	"strings"

	dockerapi "github.com/fsouza/go-dockerclient"
	"github.com/tehnerd/gnl2go"
	"gitlab.com/verraedt/go-registrator/ipvs"
)

// A Container contains information about a docker container.
type Container struct {
	Container *dockerapi.Container
	hostIPs   []net.IP
}

// A Port is a pair of a port number and a protocol.
type Port struct {
	Port  int
	Proto string
}

// A Service describes a Port that should be registered as a consul service.
type Service struct {
	ID          string
	Name        string
	Tags        []string
	Meta        map[string]string
	Port        Port
	Address     net.IP
	Checks      []Check
	ContainerID string
}

// A Check contains information about how it can be checked that a Service is healthy.
type Check struct {
	ID       string
	HTTP     string
	TCP      string
	Interval string
}

// InspectContainer looks up a docker container, given its identifier.
func (reg *Registrator) InspectContainer(id string) (*Container, error) {
	container, err := reg.docker.InspectContainerWithOptions(dockerapi.InspectContainerOptions{
		ID:      id,
		Context: reg.Deadline(),
	})
	if err != nil {
		return nil, err
	}

	c := &Container{
		Container: container,
	}

	for name := range c.Container.NetworkSettings.Networks {
		if name == "host" {
			c.hostIPs = reg.IPs
		}
	}

	return c, nil
}

// DefaultName returns the default name to give to a service, if no specific name is given.
func (c *Container) DefaultName() string {
	image := c.Container.Config.Image
	parts := strings.Split(image, "/")
	image = parts[len(parts)-1]          // strip long path
	image = strings.Split(image, ":")[0] // strip tag

	return image
}

// VirtualIPs returns a list of ips the container should listen on.
func (c *Container) VirtualIPs() map[int]net.IP {
	ips := map[int]net.IP{}

	for version := range c.IPs() {
		ipStr := c.lookupKeys([]string{fmt.Sprintf("LISTEN_IPV%d", version)}, "")

		if ipStr == "" {
			continue
		}

		ip := net.ParseIP(ipStr)
		if ip == nil {
			continue
		}

		if (version == 4 && ip.To4() == nil) || (version == 6 && ip.To4() != nil) {
			continue
		}

		ips[version] = ip
	}

	return ips
}

// IPs returns a list of ips for a container.
func (c *Container) IPs() map[int]net.IP {
	result := map[int]net.IP{}

	if c.hostIPs != nil {
		for _, ip := range c.hostIPs {
			if ip.To4() != nil {
				result[4] = ip
			} else if ip != nil {
				result[6] = ip
			}
		}

		return result
	}

	for _, network := range c.Container.NetworkSettings.Networks {
		if network.IPAddress != "" {
			ip := net.ParseIP(network.IPAddress)
			if ip != nil {
				result[4] = ip
			}
		}

		if network.GlobalIPv6Address != "" {
			ip := net.ParseIP(network.GlobalIPv6Address)
			if ip != nil {
				result[6] = ip
			}
		}
	}

	return result
}

// Ports returns a list of ports that are exposed on the container.
func (c *Container) Ports() []Port {
	ports := []Port{}

	dockerPorts := c.Container.NetworkSettings.Ports

	// In case the host network is used, we need to look up the config as NetworkSettings.Ports will not be populated.
	if c.hostIPs != nil {
		for port := range c.Container.Config.ExposedPorts {
			dockerPorts[port] = nil
		}
	}

	for port := range dockerPorts {
		number, err := strconv.Atoi(port.Port())
		if err == nil {
			ports = append(ports, Port{
				Port:  number,
				Proto: port.Proto(),
			})
		}
	}

	// Append ports that are defined by the PORTS environment variable.
	envPorts := c.lookupKeys([]string{"PORTS"}, "")

	if envPorts != "" {
		for _, portDef := range strings.Split(envPorts, ",") {
			parts := strings.Split(portDef, "/")
			proto := "tcp"

			if len(parts) > 1 {
				proto = parts[1]
			}

			number, err := strconv.Atoi(parts[0])
			if err == nil {
				ports = append(ports, Port{
					Port:  number,
					Proto: proto,
				})
			}
		}
	}

	return ports
}

// PortCount counts the different port numbers that are exposed on the container (TCP and udp count as one if they are both present).
func (c *Container) PortCount() int {
	ports := map[int]bool{}
	for _, port := range c.Ports() {
		ports[port.Port] = true
	}

	return len(ports)
}

// HasHealthCheck returns whether the container has a configured docker health check.
func (c *Container) HasHealthCheck() bool {
	return c.Container.Config.Healthcheck != nil
}

// Services list all services that should be registered in consul for a given container. The prefix argument will be prepended to the service IDs.
func (c *Container) Services(prefix string) []*Service { //nolint:funlen,gocognit
	numberOfPorts := c.PortCount()

	result := []*Service{}

	ips := c.IPs()
	virtualIPs := c.VirtualIPs()

	for _, sPort := range c.Ports() {
		for version, sAddr := range ips {
			sProto := c.serviceProto(sPort, version, sPort.Proto)
			if sProto != sPort.Proto && sProto != "both" {
				continue
			}

			result = append(result, c.buildService(prefix, "", sAddr, sPort, version, numberOfPorts > 1))

			sAddr, ok := virtualIPs[version]
			if !ok {
				continue
			}

			result = append(result, c.buildService(prefix, "-lb", sAddr, sPort, version, numberOfPorts > 1))
		}
	}

	return result
}

func (c *Container) buildService(prefix string, suffix string, sAddr net.IP, sPort Port, version int, addPortInName bool) *Service {
	id := fmt.Sprintf("%s:%s:%d:%s:ipv%d%s", prefix, c.Container.ID[:12], sPort.Port, sPort.Proto, version, suffix)

	HTTPCheck := c.serviceField("CHECK_HTTP", sPort, version, "")
	TCPCheck := c.serviceField("CHECK_TCP", sPort, version, "")
	autoCheck := c.serviceField("CHECK", sPort, version, "")
	checkInterval := c.serviceField("CHECK_INTERVAL", sPort, version, "60s")

	if HTTPCheck == "" && autoCheck == "HTTP" {
		if version == 4 {
			HTTPCheck = fmt.Sprintf("HTTP://%s:%d", sAddr, sPort.Port)
		} else {
			HTTPCheck = fmt.Sprintf("HTTP://[%s]:%d", sAddr, sPort.Port)
		}
	}

	if TCPCheck == "" && autoCheck == "TCP" {
		if version == 4 {
			TCPCheck = fmt.Sprintf("%s:%d", sAddr, sPort.Port)
		} else {
			TCPCheck = fmt.Sprintf("[%s]:%d", sAddr, sPort.Port)
		}
	}

	checks := []Check{}

	if HTTPCheck != "" {
		checks = append(checks, Check{
			ID:       fmt.Sprintf("%s-HTTP", id),
			HTTP:     HTTPCheck,
			Interval: checkInterval,
		})
	}

	if TCPCheck != "" {
		checks = append(checks, Check{
			ID:       fmt.Sprintf("%s-HTTP", id),
			TCP:      TCPCheck,
			Interval: checkInterval,
		})
	}

	if c.HasHealthCheck() {
		checks = append(checks, Check{
			ID:       fmt.Sprintf("%s-health", id),
			HTTP:     fmt.Sprintf("HTTP://localhost:8505/%s", c.Container.ID),
			Interval: checkInterval,
		})
	}

	return &Service{
		ID:          id,
		Name:        c.serviceName(sPort, version, addPortInName) + suffix,
		Tags:        c.serviceTags(sPort, version),
		Meta:        c.serviceMeta(sPort, version),
		Address:     sAddr,
		Port:        sPort,
		Checks:      checks,
		ContainerID: c.Container.ID,
	}
}

func (c *Container) LBServices() []*LBService {
	result := []*LBService{}

	ips := c.IPs()

	for version, listenIP := range c.VirtualIPs() {
		for _, port := range c.Ports() {
			result = append(result, &LBService{
				ContainerID: c.Container.ID,
				Service: ipvs.Service{
					Address:     ips[version],
					Port:        uint16(port.Port),
					Protocol:    port.Proto,
					Method:      gnl2go.IPVS_MASQUERADING,
					LBAddress:   listenIP,
					LBPort:      uint16(port.Port),
					LBMethod:    "rr",
					LBWeight:    100,
					LBVirtualIP: true,
				},
			})
		}
	}

	return result
}

func (c *Container) lookupKeys(keys []string, fallback string) string {
	value := fallback

	for _, key := range keys {
		pre := fmt.Sprintf("%s=", key)

		for _, env := range c.Container.Config.Env {
			if strings.HasPrefix(env, pre) {
				parts := strings.SplitN(env, "=", 2)
				value = parts[1]
			}
		}
	}

	return value
}

func (c *Container) serviceProto(port Port, version int, fallback string) string {
	keys := []string{
		"SERVICE",
		fmt.Sprintf("SERVICE_IPV%d", version),
		fmt.Sprintf("SERVICE_%d", port.Port),
		fmt.Sprintf("SERVICE_%d_IPV%d", port.Port, version),
	}

	return c.lookupKeys(keys, fallback)
}

func (c *Container) serviceName(port Port, version int, multiplePorts bool) string {
	keys := []string{
		"SERVICE_NAME",
		fmt.Sprintf("SERVICE_NAME_IPV%d", version),
	}

	fallback := c.lookupKeys(keys, c.DefaultName())

	if multiplePorts {
		fallback = fmt.Sprintf("%s-%d", fallback, port.Port)
	}

	keys = []string{
		fmt.Sprintf("SERVICE_%d_NAME", port.Port),
		fmt.Sprintf("SERVICE_%d_NAME_IPV%d", port.Port, version),
	}

	return c.lookupKeys(keys, fallback)
}

func (c *Container) serviceTags(port Port, version int) []string {
	tagsStr := c.serviceField("TAGS", port, version, "")
	tags := strings.Split(tagsStr, ",")

	if port.Proto != "TCP" {
		tags = append(tags, port.Proto)
	}

	// Append fw.internal if no fw. tags are set
	hasFWtags := false

	for _, tag := range tags {
		if len(tag) > 3 && tag[0:3] == "fw." {
			hasFWtags = true
		}
	}

	if !hasFWtags {
		tags = append(tags, "fw.internal")
	}

	for k, v := range c.Container.Config.Labels {
		tags = append(tags, strings.ReplaceAll(fmt.Sprintf("%s=%s", k, v), "{}", fmt.Sprintf("%d", port.Port)))
	}

	return tags
}

func (c *Container) serviceMeta(port Port, version int) map[string]string {
	keys := []*regexp.Regexp{
		regexp.MustCompile("SERVICE_META_(?P<key>.*)=(?P<val>.*)"),
		regexp.MustCompile(`SERVICE_META_(?P<key>.*)_IPV(?P<version>\d)=(?P<val>.*)`),
		regexp.MustCompile(fmt.Sprintf("SERVICE_%d_META_(?P<key>.*)=(?P<val>.*)", port.Port)),
		regexp.MustCompile(fmt.Sprintf(`SERVICE_%d_META_(?P<key>.*)_IPV(?P<version>\d)=(?P<val>.*)`, port.Port)),
	}

	meta := map[string]string{}

	for _, env := range c.Container.Config.Env {
		var (
			key string
			val string
		)

		for _, re := range keys {
			match := re.FindStringSubmatch(env)
			if match == nil {
				continue
			}

			groups := map[string]string{}

			for i, n := range re.SubexpNames() {
				groups[n] = match[i]
			}

			if groups["version"] != "" {
				parsed, err := strconv.Atoi(groups["version"])
				if err != nil {
					continue
				} else if parsed != version {
					key = ""
					continue
				}
			}

			key = strings.ToLower(groups["key"])
			val = groups["val"]
		}

		if key != "" {
			meta[key] = val
		}
	}

	return meta
}

func (c *Container) serviceField(field string, port Port, version int, fallback string) string {
	keys := []string{
		fmt.Sprintf("SERVICE_%s", field),
		fmt.Sprintf("SERVICE_%s_IPV%d", field, version),
		fmt.Sprintf("SERVICE_%d_%s", port.Port, field),
		fmt.Sprintf("SERVICE_%d_%s_IPV%d", port.Port, field, version),
	}

	return c.lookupKeys(keys, fallback)
}
