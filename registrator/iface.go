package registrator

import (
	"errors"
	"fmt"
	"net"

	"github.com/sirupsen/logrus"
	"gitlab.com/verraedt/go-registrator/util"
)

var ErrNoIP = errors.New("no valid ip")

func (reg *Registrator) AddressListener() (<-chan []net.IP, error) {
	var (
		static4, static6 bool
		staticAddresses  []net.IP
	)

	// Static ip addresses
	for _, addr := range reg.Config.PublicAddresses {
		ip := net.ParseIP(addr)
		if ip == nil {
			return nil, fmt.Errorf("%w: %s", ErrNoIP, addr)
		}

		if ip.To4() != nil {
			static4 = true
		} else {
			static6 = true
		}

		staticAddresses = append(staticAddresses, ip)
	}

	ch := make(chan []net.IP, 1)

	var addressListener *util.AddressListener

	switch {
	case static4 && static6:
		ch <- staticAddresses

		return ch, nil
	case reg.Config.PublicInterface != "":
		iface, err := net.InterfaceByName(reg.Config.PublicInterface)
		if err != nil || iface == nil {
			return nil, err
		}

		addressListener, err = util.NewAddressListenerIfaces(reg.Context, []net.Interface{*iface})
		if err != nil {
			return nil, err
		}
	default:
		var err error

		addressListener, err = util.NewAddressListener(reg.Context)
		if err != nil {
			return nil, err
		}
	}

	go reg.dispatchChangedAddress(addressListener.Channel, ch, staticAddresses, static4, static6)

	return ch, nil
}

func (reg *Registrator) dispatchChangedAddress(in <-chan []net.IP, out chan<- []net.IP, static []net.IP, static4, static6 bool) {
	defer close(out)

	for {
		detectedIPs, ok := <-in
		if !ok {
			break
		}

		addresses := []net.IP{}

		// Filter out detected IPs that have same family as a static ip
		for _, ip := range detectedIPs {
			if (ip.To4() != nil && static4) || (ip.To4() == nil && static6) {
				continue
			}

			addresses = append(addresses, ip)
		}

		// Register discovered IPs in DNS (only non static ones)
		if err := reg.RegisterDNS(addresses); err != nil {
			logrus.Errorf("Could not update DNS: %s", err)
		}

		// Append static addresses
		addresses = append(addresses, static...)

		out <- addresses
	}
}
