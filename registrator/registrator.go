package registrator

import (
	"errors"
	"fmt"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	dockerapi "github.com/fsouza/go-dockerclient"
	consul "github.com/hashicorp/consul/api"
	"gitlab.com/verraedt/go-registrator/ipvs"
	"golang.org/x/net/context"

	log "github.com/sirupsen/logrus"
)

type Config struct {
	ConsulAddr      string
	Token           string
	Prefix          string
	Hostname        string
	SweepInterval   int
	PublicAddresses []string
	PublicInterface string
	Interface       string
	DoToken         string
	DoDomain        string
}

type Registrator struct {
	Config
	Consul     *consul.Client
	Prefix     string
	Timeout    time.Duration   // Timeout of each operation
	Context    context.Context // Global context
	docker     *dockerapi.Client
	ipvs       *ipvs.IPVS
	services   []*Service
	lbservices []*LBService
	IPs        []net.IP
}

type LBService struct {
	ContainerID string
	ipvs.Service
}

// New registrator.
func New(config Config) (*Registrator, error) {
	consulConfig := consul.DefaultConfig()

	if config.ConsulAddr != "" {
		consulConfig.Address = config.ConsulAddr
	}

	if config.Token != "" {
		consulConfig.Token = config.Token
	}

	consulClient, err := consul.NewClient(consulConfig)
	if err != nil {
		return nil, err
	}

	docker, err := dockerapi.NewClientFromEnv()
	if err != nil {
		return nil, err
	}

	ipvs, err := ipvs.NewIPVS(&ipvs.Config{
		Interface: config.Interface,
	})
	if err != nil {
		return nil, err
	}

	return &Registrator{
		Config:  config,
		Consul:  consulClient,
		Prefix:  fmt.Sprintf("%s:%s", config.Prefix, config.Hostname),
		Timeout: 5 * time.Second,
		Context: context.Background(),
		docker:  docker,
		ipvs:    ipvs,
	}, nil
}

var ErrChannelClosed = errors.New("docker channel is closed")

func (reg *Registrator) listenDocker() (<-chan *dockerapi.APIEvents, error) {
	dockerEvents := make(chan *dockerapi.APIEvents, 100)

	opts := dockerapi.EventsOptions{
		Filters: map[string][]string{"type": {"container"}, "event": {"start", "die"}},
	}

	if err := reg.docker.AddEventListenerWithOptions(opts, dockerEvents); err != nil {
		return nil, err
	}

	return dockerEvents, nil
}

func (reg *Registrator) Close() error {
	return reg.ipvs.Close()
}

// Run the registrator loop.
func (reg *Registrator) Run() error {
	var (
		osSignals = make(chan os.Signal, 1)
		ticker    = time.NewTicker(time.Duration(reg.SweepInterval) * time.Second)
	)

	defer ticker.Stop()

	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)

	defer signal.Stop(osSignals)

	dockerEvents, err := reg.listenDocker()
	if err != nil {
		return err
	}

	addr, err := reg.AddressListener()
	if err != nil {
		return err
	}

	reg.IPs = <-addr

	// Don't trust the dockerEvents channel for first seconds
	// The event listening is started up in the background and events might be missed at startup.
	// We fix this by doing the first sweep early
	time.AfterFunc(10*time.Second, func() {
		osSignals <- syscall.SIGHUP
	})

	if err = reg.Sweep(); err != nil {
		return err
	}

	for {
		err = reg.UpdateLB()
		if err != nil {
			return err
		}

		select {
		case newIPs := <-addr:
			reg.IPs = newIPs

			err = reg.Sweep()

		case msg, active := <-dockerEvents:
			if !active {
				return ErrChannelClosed
			}

			switch msg.Action {
			case "start":
				err = reg.RegisterContainerID(msg.Actor.ID)
			case "die":
				err = reg.DeregisterContainerID(msg.Actor.ID)
			}

		case <-ticker.C:
			err = reg.Sweep()

		case s := <-osSignals:
			if s == syscall.SIGHUP {
				err = reg.Sweep()

				break
			}

			if err = reg.DeregisterAll(); err != nil {
				return err
			}

			return reg.Cleanup()
		}

		if err != nil {
			return err
		}
	}
}

func (reg *Registrator) Deadline() context.Context {
	ctx, _ := context.WithDeadline(reg.Context, time.Now().Add(reg.Timeout))

	return ctx
}

func (reg *Registrator) DeadlineQuery() *consul.QueryOptions {
	q := consul.QueryOptions{}

	return q.WithContext(reg.Deadline())
}

func (reg *Registrator) DeadlineServiceRegister() consul.ServiceRegisterOpts {
	q := consul.ServiceRegisterOpts{}

	return q.WithContext(reg.Deadline())
}

// RegisterContainerID registers the services for a container given by ID.
func (reg *Registrator) RegisterContainerID(id string) error {
	container, err := reg.InspectContainer(id)
	if err != nil {
		return err
	}

	return reg.RegisterContainer(container)
}

// RegisterContainer registers the services for a given Container.
func (reg *Registrator) RegisterContainer(container *Container) error {
	for _, service := range container.Services(reg.Prefix) {
		err := reg.registerService(service)
		if err != nil {
			return err
		}

		reg.services = append(reg.services, service)
	}

	reg.lbservices = append(reg.lbservices, container.LBServices()...)

	return nil
}

// DeregisterContainerID deregisters the services for a container given by ID.
func (reg *Registrator) DeregisterContainerID(id string) error {
	for i := len(reg.services) - 1; i >= 0; i-- {
		service := reg.services[i]

		if id == service.ContainerID {
			err := reg.deregisterService(service)
			if err != nil {
				return err
			}

			reg.services = append(reg.services[:i], reg.services[i+1:]...)
		}
	}

	for i := len(reg.lbservices) - 1; i >= 0; i-- {
		if id == reg.lbservices[i].ContainerID {
			reg.lbservices = append(reg.lbservices[:i], reg.lbservices[i+1:]...)
		}
	}

	return nil
}

// DeregisterAll deregisters all containers known to the registrator service.
func (reg *Registrator) DeregisterAll() error {
	for _, service := range reg.services {
		err := reg.deregisterService(service)
		if err != nil {
			return err
		}
	}

	reg.services = []*Service{}
	reg.lbservices = nil

	return nil
}

// Sweep reregisters all containers on this host, and runs Cleanup afterwards.
func (reg *Registrator) Sweep() error {
	log.Info("Start sweep")

	// Clear memory
	reg.services = nil
	reg.lbservices = nil

	// Register all containers
	filters := map[string][]string{"status": {"created", "restarting", "running", "paused"}}

	containers, err := reg.docker.ListContainers(dockerapi.ListContainersOptions{
		Filters: filters,
		Context: reg.Deadline(),
	})
	if err != nil {
		return err
	}

	for _, container := range containers {
		err = reg.RegisterContainerID(container.ID)
		if err != nil {
			return err
		}
	}

	return reg.Cleanup()
}

// Cleanup cleans up dangling services that are not known to the registrator service, but start by its prefix.
func (reg *Registrator) Cleanup() error {
	log.Info("Start cleanup")

	services, err := reg.Consul.Agent().ServicesWithFilterOpts("", reg.DeadlineQuery())
	if err != nil {
		return err
	}

	lookfor := fmt.Sprintf("%s:", reg.Prefix)

	for _, service := range services {
		if strings.HasPrefix(service.ID, lookfor) {
			known := false

			for _, knownService := range reg.services {
				if service.ID == knownService.ID {
					known = true
				}
			}

			if !known {
				log.Debugf("Cleanup old %s", service.ID)

				err = reg.Consul.Agent().ServiceDeregisterOpts(service.ID, reg.DeadlineQuery())
				if err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func (reg *Registrator) registerService(service *Service) error {
	log.Debugf("Registering %s for container %s", service.ID, service.ContainerID)

	checks := consul.AgentServiceChecks{}

	for _, check := range service.Checks {
		checks = append(checks, &consul.AgentServiceCheck{
			CheckID:  check.ID,
			HTTP:     check.HTTP,
			TCP:      check.TCP,
			Interval: check.Interval,
		})
	}

	registration := &consul.AgentServiceRegistration{
		ID:      service.ID,
		Name:    service.Name,
		Port:    service.Port.Port,
		Tags:    service.Tags,
		Address: service.Address.String(),
		Checks:  checks,
		Meta:    service.Meta,
	}

	return reg.Consul.Agent().ServiceRegisterOpts(registration, reg.DeadlineServiceRegister())
}

func (reg *Registrator) deregisterService(service *Service) error {
	log.Debugf("Unsubscribe %s for container %s", service.ID, service.ContainerID)

	return reg.Consul.Agent().ServiceDeregisterOpts(service.ID, reg.DeadlineQuery())
}

func (reg *Registrator) UpdateLB() error {
	services := []ipvs.Service{}

	for _, lbservice := range reg.lbservices {
		services = append(services, lbservice.Service)
	}

	return reg.ipvs.Update(services)
}
