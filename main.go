package main

import (
	"os"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/verraedt/go-registrator/registrator"

	log "github.com/sirupsen/logrus"
)

func main() {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}

	var (
		config registrator.Config
		debug  bool
	)

	rootCmd := &cobra.Command{
		Use:   "registrator",
		Short: "Registrates docker services in consul",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			if debug {
				log.SetLevel(log.DebugLevel)
			}

			reg, err := registrator.New(config)
			if err != nil {
				log.Fatal(err)
			}

			go func() {
				err := reg.ServeHealth()
				log.Panic(err)
			}()

			defer reg.Close()

			retry(reg.Run, 10)
		},
	}

	rootCmd.Flags().StringVar(&config.ConsulAddr, "consul", "http://localhost:8500", "Address of the consul service")
	rootCmd.Flags().StringVar(&config.Token, "token", "", "Consul ACL token")
	rootCmd.Flags().StringVar(&config.Hostname, "hostname", hostname, "Hostname used as part of the container consul identifiers")
	rootCmd.Flags().StringVar(&config.PublicInterface, "interface", "", "Interface to query for public ip addresses, used for registrating containers with host networking")
	rootCmd.Flags().StringSliceVar(&config.PublicAddresses, "address", nil, "Staticly assigned public ip addresses, can be used to override autodetected public addresses or to assign internal ips to which port forwarding is set up")
	rootCmd.Flags().StringVar(&config.Prefix, "prefix", "goregistrator", "Service prefix to register services under")
	rootCmd.Flags().IntVar(&config.SweepInterval, "sweep-interval", 3600, "Sweep interval expressed in seconds")
	rootCmd.Flags().StringVar(&config.Interface, "lb-interface", "docker0", "Interface for registration of virtual ip addresses")
	rootCmd.Flags().StringVar(&config.DoToken, "do-token", os.Getenv("DIGITALOCEAN_TOKEN"), "Digitalocean API token, if empty, dns will not be updated")
	rootCmd.Flags().StringVar(&config.DoDomain, "do-domain", "ext.verraedt.be", "Digitalocean domain")
	rootCmd.Flags().BoolVarP(&debug, "debug", "v", false, "Verbose output")

	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

type retryable func() error

func retry(callback retryable, maxFailures int) {
	var failures int

	for {
		err := callback()
		if err == nil {
			return
		}

		failures++

		if maxFailures > 0 && failures > maxFailures {
			log.Fatalf("%s, max retries reached.", err)
		}

		timeout := failures * failures
		if timeout > 625 {
			timeout = 625
		}

		log.Warnf("%s, sleeping %d seconds.", err, timeout)
		time.Sleep(time.Duration(timeout) * time.Second)
	}
}
