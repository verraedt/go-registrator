package ipvs

import (
	"fmt"
	"io/ioutil"
	"net"
	"os/exec"
	"syscall"
	"time"

	"github.com/tehnerd/gnl2go"
	"github.com/vishvananda/netlink"

	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
)

// Config represents config for a IPVS handler.
type Config struct {
	// Interface to configure local addresses on
	Interface string
}

// A IPVS session.
type IPVS struct {
	IpvsClient *gnl2go.IpvsClient
	Link       netlink.Link
	balancers  []*Balancer
	virtualIPs []net.IP
	*Config
}

// NewIPVS prepares a new IPVS handler.
func NewIPVS(config *Config) (*IPVS, error) {
	if err := exec.Command("/sbin/modprobe", "ip_vs").Run(); err != nil {
		return nil, err
	}

	if err := ioutil.WriteFile("/proc/sys/net/ipv4/vs/conntrack", []byte("1"), 0o600); err != nil {
		return nil, err
	}

	ipvsClient := new(gnl2go.IpvsClient)

	if err := ipvsClient.Init(); err != nil {
		return nil, err
	}

	if err := ipvsClient.Flush(); err != nil {
		return nil, err
	}

	link, err := netlink.LinkByName(config.Interface)
	if err != nil {
		return nil, err
	}

	return &IPVS{
		IpvsClient: ipvsClient,
		Link:       link,
		Config:     config,
	}, nil
}

// A Balancer for the dynamic firewall load balancer, to indicate the current state.
type Balancer struct {
	Addr         net.IP
	Port         uint16
	Protocol     uint16
	Method       string
	VirtualIP    bool
	Backends     []*Backend
	FirewallTags []string // Tags to be added to the consul service registration
}

func (b *Balancer) String() string {
	var protocol string

	switch b.Protocol {
	case syscall.IPPROTO_TCP:
		protocol = "tcp"
	case syscall.IPPROTO_UDP:
		protocol = "udp"
	default:
		protocol = fmt.Sprintf("%d", b.Protocol)
	}

	return fmt.Sprintf("%s:%d %s (method %s)", b.Addr, b.Port, protocol, b.Method)
}

// A Backend for the dynamic firewall load balancer, to indicate the current state.
type Backend struct {
	Addr     net.IP
	Port     uint16
	Method   uint32
	Weight   int32
	Balancer *Balancer
}

func (b *Backend) String() string {
	return fmt.Sprintf("%s -> %s:%d (method %d weight %d)", b.Balancer.String(), b.Addr, b.Port, b.Method, b.Weight)
}

// Equal function.
func (b *Balancer) Equal(other *Balancer) bool {
	return b.Addr.Equal(other.Addr) && b.Port == other.Port && b.Protocol == other.Protocol
}

// Hash calculates a unique hash of a service.
func (b *Backend) Hash() string {
	return fmt.Sprintf("%s:%d", b.Addr, b.Port)
}

// Equal function.
func (b *Backend) Equal(other *Backend) bool {
	return b.Addr.Equal(other.Addr) && b.Port == other.Port
}

// Update services.
func (i *IPVS) Update(services []Service) error { //nolint:funlen,gocognit
	oldBackends := i.getCurrentBackends()
	oldIPs := i.getCurrentIPs()

	var err error

	for _, service := range services {
		balancer := &Balancer{
			Addr:      service.LBAddress,
			Port:      service.LBPort,
			Protocol:  uint16(gnl2go.ToProtoNum(gnl2go.NulStringType(service.Protocol))),
			Method:    service.LBMethod,
			VirtualIP: service.LBVirtualIP,
		}

		balancer, err = i.registerBalancer(balancer)
		if err != nil {
			return err
		}

		backend := &Backend{
			Addr:     service.Address,
			Port:     service.Port,
			Method:   service.Method,
			Weight:   100,
			Balancer: balancer,
		}

		backend, err = i.registerBackend(backend)
		if err != nil {
			return err
		}

		if balancer.VirtualIP {
			if err = i.registerIP(service.LBAddress, service.Method); err != nil {
				return err
			}
		}

		delete(oldBackends, backend.Hash())
		delete(oldIPs, service.LBAddress.String())
	}

	for _, ip := range oldIPs {
		if err = i.deregisterIP(ip); err != nil {
			return err
		}
	}

	for _, backend := range oldBackends {
		if err = i.deregisterBackend(backend); err != nil {
			return err
		}
	}

	for _, balancer := range i.balancers {
		if len(balancer.Backends) > 0 {
			continue
		}

		if err = i.deregisterBalancer(balancer); err != nil {
			return err
		}
	}

	return nil
}

func (i *IPVS) getCurrentBackends() map[string]*Backend {
	result := map[string]*Backend{}

	for _, balancer := range i.balancers {
		for _, backend := range balancer.Backends {
			result[backend.Hash()] = backend
		}
	}

	return result
}

func (i *IPVS) getCurrentIPs() map[string]net.IP {
	result := map[string]net.IP{}

	for _, ip := range i.virtualIPs {
		result[ip.String()] = ip
	}

	return result
}

func (i *IPVS) registerBalancer(balancer *Balancer) (*Balancer, error) {
	for _, currentBalancer := range i.balancers {
		if balancer.Equal(currentBalancer) {
			return currentBalancer, nil
		}
	}

	log.Printf("IPVS: Creating balancer %s", balancer)

	// Create balancer
	if err := i.IpvsClient.AddService(balancer.Addr.String(), balancer.Port, balancer.Protocol, balancer.Method); err != nil {
		return nil, err
	}

	// Append balancer
	balancer.Backends = []*Backend{}

	i.balancers = append(i.balancers, balancer)

	return balancer, nil
}

func (i *IPVS) registerBackend(backend *Backend) (*Backend, error) {
	var err error

	for _, currentBackend := range backend.Balancer.Backends {
		if !currentBackend.Equal(backend) {
			continue
		}

		if backend.Weight == currentBackend.Weight && backend.Method == currentBackend.Method {
			return currentBackend, nil
		}

		log.Printf("IPVS: Updating backend %s", backend)

		err = i.IpvsClient.UpdateDestPort(backend.Balancer.Addr.String(), backend.Balancer.Port, backend.Addr.String(), backend.Port, backend.Balancer.Protocol, backend.Weight, backend.Method)
		if err != nil {
			return nil, err
		}

		currentBackend.Weight = backend.Weight
		currentBackend.Method = backend.Method

		return currentBackend, nil
	}

	log.Printf("IPVS: Creating backend %s", backend)

	// Add backend
	err = i.IpvsClient.AddDestPort(backend.Balancer.Addr.String(), backend.Balancer.Port, backend.Addr.String(), backend.Port, backend.Balancer.Protocol, backend.Weight, backend.Method)
	if err != nil {
		return nil, err
	}

	// Append backend
	backend.Balancer.Backends = append(backend.Balancer.Backends, backend)

	return backend, nil
}

func (i *IPVS) deregisterBackend(backend *Backend) error {
	log.Printf("IPVS: Removing backend %s", backend)

	err := i.IpvsClient.DelDestPort(backend.Balancer.Addr.String(), backend.Balancer.Port, backend.Addr.String(), backend.Port, backend.Balancer.Protocol)
	if err != nil {
		return err
	}

	// Remove backend from balancer
	for p, oldBackend := range backend.Balancer.Backends {
		if oldBackend == backend {
			backend.Balancer.Backends = append(backend.Balancer.Backends[:p], backend.Balancer.Backends[p+1:]...)
			break
		}
	}

	return nil
}

func (i *IPVS) deregisterBalancer(balancer *Balancer) error {
	log.Printf("IPVS: Removing balancer %s", balancer)

	// If this happens too fast, this might fail, retry a couple of times
	count := 0

	for {
		err := i.IpvsClient.DelService(balancer.Addr.String(), balancer.Port, balancer.Protocol)
		if err == nil {
			break
		}

		count++

		if count > 3 {
			logrus.Errorf("deleting ipvs balancer failed, ignoring: %s", err)

			return nil
		}

		logrus.Warnf("deleting ipvs balancer failed, will retry: %s", err)

		time.Sleep(time.Second)
	}

	for p, oldBalancer := range i.balancers {
		if oldBalancer.Equal(balancer) {
			i.balancers = append(i.balancers[:p], i.balancers[p+1:]...)
			break
		}
	}

	return nil
}

func (i *IPVS) registerIP(ip net.IP, method uint32) error {
	for _, currentIP := range i.virtualIPs {
		if currentIP.Equal(ip) {
			return nil
		}
	}

	log.Printf("IPVS: Registering virtual ip %s", ip)

	var (
		cidr   string
		family int
	)

	if ip.To4() != nil {
		cidr = fmt.Sprintf("%s/32", ip.String())
		family = netlink.FAMILY_V4
	} else {
		cidr = fmt.Sprintf("%s/128", ip.String())
		family = netlink.FAMILY_V6
	}

	addr, err := netlink.ParseAddr(cidr)
	if err != nil {
		return err
	}

	err = netlink.AddrReplace(i.Link, addr)
	if err != nil {
		return err
	}

	i.virtualIPs = append(i.virtualIPs, ip)

	if method == gnl2go.IPVS_MASQUERADING {
		return nil
	}

	var localIP net.IP

	localIP, err = i.findLocalIP(family)
	if err != nil || localIP == nil {
		log.Printf("Could not find local ip on interface %s: %s", i.Interface, err)

		return nil
	}

	route := &netlink.Route{
		LinkIndex: i.Link.Attrs().Index,
		Table:     255, // local
		Dst:       addr.IPNet,
		Src:       localIP,
	}

	return netlink.RouteReplace(route)
}

func (i *IPVS) findLocalIP(family int) (net.IP, error) {
	addrs, err := netlink.AddrList(i.Link, family)
	if err != nil {
		return nil, err
	}

	for _, addr := range addrs {
		if a, b := addr.IPNet.Mask.Size(); a >= b {
			continue
		}

		if addr.IP.IsGlobalUnicast() {
			return addr.IP, nil
		}
	}

	return nil, nil
}

func (i *IPVS) deregisterIP(ip net.IP) error {
	log.Printf("IPVS: Removing virtual ip %s\n", ip)

	var cidr string

	if ip.To4() != nil {
		cidr = fmt.Sprintf("%s/32", ip.String())
	} else {
		cidr = fmt.Sprintf("%s/128", ip.String())
	}

	addr, err := netlink.ParseAddr(cidr)
	if err != nil {
		return err
	}

	link, err := netlink.LinkByName(i.Interface)
	if err != nil {
		return err
	}

	err = netlink.AddrDel(link, addr)
	if err != nil {
		return err
	}

	for p, currentIP := range i.virtualIPs {
		if currentIP.Equal(ip) {
			i.virtualIPs = append(i.virtualIPs[:p], i.virtualIPs[p+1:]...)
			break
		}
	}

	return nil
}

// Close exists the client.
func (i *IPVS) Close() error {
	defer i.IpvsClient.Exit()

	log.Println("Closing IPVS")

	for _, ip := range i.getCurrentIPs() {
		if err := i.deregisterIP(ip); err != nil {
			return err
		}
	}

	return nil
}
