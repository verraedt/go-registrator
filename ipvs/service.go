package ipvs

import (
	"fmt"
	"net"
)

// A Service defines a load balanced service.
type Service struct {
	Address     net.IP
	Port        uint16
	Protocol    string
	Method      uint32
	LBAddress   net.IP
	LBPort      uint16
	LBMethod    string
	LBWeight    int32
	LBVirtualIP bool
}

func (s *Service) String() string {
	return fmt.Sprintf("%s:%d %s -> %s:%d (lb-method %s weight %d method %d virtualIP %v)", s.LBAddress, s.LBPort, s.Protocol, s.Address, s.Port, s.LBMethod, s.LBWeight, s.Method, s.LBVirtualIP)
}
